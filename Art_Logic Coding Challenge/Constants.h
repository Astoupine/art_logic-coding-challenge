// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#pragma once

#define kOP_CODE_CLR "F0"
#define kOP_CODE_PEN "80"
#define kOP_CODE_CO "A0"
#define kOP_CODE_MV "C0"

#define kMAX_VALID_COORDINATE 8191
#define kMIN_VALID_COORDINATE -8192