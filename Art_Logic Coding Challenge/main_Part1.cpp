// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

/*
#include <fstream>
#include <iostream>

#include "Codec.h"

using namespace std;

int main()
{
	//Get input data from file 
	string line;
	ifstream inFile("InputData.txt");
	ofstream outFile;
	if (inFile.is_open())
	{
		ACodec codec;
		outFile.open("ConvertedData.txt");

		while (getline(inFile, line))
		{
			outFile << "Input: " << line << '\n';

			if (line.at(0) == '\"') //All hex values begin and end with a quote
			{
				//This is a hex value to decode
				outFile << "Decoding" << '\n';
				if (line.length() == 6) //If we have 4 hex digits and 2 quote characters, this is a valid input
				{
					//Breaking hex value string up into high and low bytes
					string hiByte = line.substr(1, 2);
					string loByte = line.substr(3, 2);

					//Making sure we have a valid hex value
					bool isHiHex = (hiByte.find_first_not_of("0123456789ABCDEFabcdef") == std::string::npos);
					bool isLoHex = (loByte.find_first_not_of("0123456789ABCDEFabcdef") == std::string::npos);

					if (isHiHex && isLoHex)
					{
						outFile << "Decoded value: " << codec.Decode(hiByte, loByte) << '\n';
					}
					else
					{
						outFile << "Input is not a valid hex value - skipping";
					}
				}
				else
				{
					outFile << "This is not a valid hex value - skipping" << '\n';
				}
			}
			else
			{
				//This is an integer to encode
				outFile << "Encoding" << '\n';

				bool isAllDigits = (line.find_first_not_of("-0123456789") == std::string::npos);

				if (isAllDigits)
				{
					int value = atoi(line.c_str());
					bool isInRange = value >= -8192 && value <= 8191;

					if (isInRange)
					{
						outFile << "Encoded value: " << codec.Encode(value) << '\n';
					}
					else
					{
						outFile << "Integer is out of range [-8192, 8191] - skipping" << '\n';
					}
				}
				else
				{
					outFile << "This is not a valid integer value - skipping" << '\n';
				}
			}

			outFile << '\n';
		}
		inFile.close();
		outFile.close();
	}
	else
	{
		cout << "Error: Unable to open input data file.";
	}
}
*/