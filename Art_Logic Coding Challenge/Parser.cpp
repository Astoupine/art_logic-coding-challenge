// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#include "Parser.h"

using namespace std;

vector<ACommand>* AParser::Parse(string input)
{
   vector<ACommand>* commands = new vector<ACommand>();
   ACodec codec;

   int currentXCoord = 0;
   int currentYCoord = 0;
   bool isPenRaised = true;

   while (input != "")
   {
	   //Looking for an OP Code
	   string opCodeCheck = input.substr(0, 2);

	   if (this->IsStringOpCode(opCodeCheck))
      {
         input = input.substr(2);
      }

      if (opCodeCheck == kOP_CODE_CLR)
      {
         //Clearing all output commands
         commands->clear();

         AClearCommand clrCommand;
         clrCommand.PrepareOutputString();
         commands->push_back(clrCommand);

         //Set the pen coordinates to the origin if it has moved
         if (currentXCoord != 0 || currentYCoord != 0)
         {
	         currentXCoord = 0;
	         currentYCoord = 0;
         }

         //Reset the pen if it is down
         if (!isPenRaised)
         {
	         isPenRaised = true;
         }
      }
      else if (opCodeCheck == kOP_CODE_PEN)
      {
         string param = input.substr(0, 4);
         int boolVal = codec.Decode(param.substr(0, 2), param.substr(2));
         input = input.substr(4);

         int isPenDown = boolVal;

         APenCommand penCommand(isPenDown);
         penCommand.PrepareOutputString();
         commands->push_back(penCommand);
      }
      else if (opCodeCheck == kOP_CODE_CO)
      {
         //Color has 4 parameters (4 letters each), each of which need to be decoded into an integer value
         string redParam = input.substr(0, 4);
         int redValue = codec.Decode(redParam.substr(0,2), redParam.substr(2));
         input = input.substr(4);

         string greenParam = input.substr(0, 4);
         int greenValue = codec.Decode(greenParam.substr(0, 2), greenParam.substr(2));
         input = input.substr(4);

         string blueParam = input.substr(0, 4);
         int blueValue = codec.Decode(blueParam.substr(0, 2), blueParam.substr(2));
         input = input.substr(4);

         string alphaParam = input.substr(0, 4);
         int alphaValue = codec.Decode(alphaParam.substr(0, 2), alphaParam.substr(2));
         input = input.substr(4);

         AColorCommand colorCommand(redValue, greenValue, blueValue, alphaValue);
         colorCommand.PrepareOutputString();
         commands->push_back(colorCommand);
      }
      else if (opCodeCheck == kOP_CODE_MV)
      {
         vector<ACommand>* modifiedCommands = new vector<ACommand>();
         vector<pair<int, int>> points;
         
         string innerOpCodeCheck = "";		
         bool isPenRaisedForOutOfBounds = false;

         bool wasYInterceptFound = false;
         bool wasXInterceptFound = false;

         bool isFirstPass = true;

         while (!this->IsStringOpCode(innerOpCodeCheck))
         {
            //After getting an x and y coordinate, we need to check for garbage values 
            //To do that we find the first occurrence of the next opCode
            //If the distance to the next opCode is divisible by 8 (4 for x, 4 for y), 
            //then we keep reading in coordinates. Otherwise, it is garbage and ignored
            if (!isFirstPass)
            {
	            int distance = this->CalcDistanceToNextOpCode(input);
	            if (distance % 8 != 0)
	            {
		            input = input.substr(distance);
		            break;
	            }
            }
            else
            {
	            isFirstPass = false;
            }

            string xCoordEncoded = input.substr(0, 4);
            int xCoordDecoded = codec.Decode(xCoordEncoded.substr(0, 2), xCoordEncoded.substr(2));
            currentXCoord += xCoordDecoded;
            input = input.substr(4);

            string yCoordEncoded = input.substr(0, 4);
            int yCoordDecoded = codec.Decode(yCoordEncoded.substr(0, 2), yCoordEncoded.substr(2));
            currentYCoord += yCoordDecoded;
            input = input.substr(4);

            int modifiedXCoord = currentXCoord;
            int modifiedYCoord = currentYCoord;
				
            bool outOfBounds = false;
            if (currentXCoord > kMAX_VALID_COORDINATE || currentXCoord < kMIN_VALID_COORDINATE)
            {
               modifiedXCoord = currentXCoord > 0 ? kMAX_VALID_COORDINATE : kMIN_VALID_COORDINATE;
               outOfBounds = true;

               //If we have a sloped line, we need to find the Y value of the point at which the line intersects the bounds
               //We can use properties of similar triangles to do this
               if (yCoordDecoded != 0 && !isPenRaisedForOutOfBounds)
               {
                  modifiedYCoord = this->FindYPointIntersectingBounds(currentXCoord - xCoordDecoded, 
                     currentYCoord - yCoordDecoded, currentXCoord, currentYCoord);
                  wasYInterceptFound = true;
               }
            }

            if (currentYCoord > kMAX_VALID_COORDINATE || currentYCoord < kMIN_VALID_COORDINATE)
            {
               modifiedYCoord = currentYCoord > 0 ? kMAX_VALID_COORDINATE : kMIN_VALID_COORDINATE;
               outOfBounds = true;

               //If we have a sloped line, we need to find the X value of the point at which the line intersects the bounds
               //We can use properties of similar triangles to do this
               if (xCoordDecoded != 0 && !isPenRaisedForOutOfBounds)
               {
                  modifiedXCoord = this->FindXPointIntersectingBounds(currentXCoord - xCoordDecoded, 
                     currentYCoord - yCoordDecoded, currentXCoord, currentYCoord);
                  wasXInterceptFound = true;
               }
            }

            if (outOfBounds && !isPenRaisedForOutOfBounds) //We are out of bounds
            {
               AMoveCommand updatedMoveCommand;
               updatedMoveCommand.AddPoint(modifiedXCoord, modifiedYCoord);
               updatedMoveCommand.PrepareOutputString();

               APenCommand penCommand(0);
               penCommand.PrepareOutputString();
               isPenRaisedForOutOfBounds = true;
               isPenRaised = true;

               modifiedCommands->push_back(updatedMoveCommand);
               modifiedCommands->push_back(penCommand);
            }
            else if (isPenRaisedForOutOfBounds && !outOfBounds) //We are back in bounds
            {
               //Calculate the intersecting point again when coming back into bounds
               if (wasXInterceptFound || wasYInterceptFound)
               {
                  AMoveCommand comingOutOfBoundsMoveCommand;

                  if (wasXInterceptFound)
                  {
                     int minOrMaxCoord = currentXCoord < 0 ? kMIN_VALID_COORDINATE : kMAX_VALID_COORDINATE;
                     int newX = this->FindXPointIntersectingBounds(currentXCoord - xCoordDecoded, 
                        currentYCoord - yCoordDecoded, currentXCoord, currentYCoord);
                     comingOutOfBoundsMoveCommand.AddPoint(newX, minOrMaxCoord);
                     wasXInterceptFound = false;
                  }
                  else if (wasYInterceptFound)
                  {
                     int minOrMaxCoord = currentYCoord < 0 ? kMIN_VALID_COORDINATE : kMAX_VALID_COORDINATE;
                     int newY = this->FindYPointIntersectingBounds(currentXCoord - xCoordDecoded,
                        currentYCoord - yCoordDecoded, currentXCoord, currentYCoord);
                     comingOutOfBoundsMoveCommand.AddPoint(minOrMaxCoord, newY);
                     wasYInterceptFound = false;
                  }

                  comingOutOfBoundsMoveCommand.PrepareOutputString();
                  modifiedCommands->push_back(comingOutOfBoundsMoveCommand);
               }

               APenCommand penCommand(1);
               penCommand.PrepareOutputString();
               isPenRaisedForOutOfBounds = false;
               isPenRaised = false;

               AMoveCommand updatedMoveCommand;
               updatedMoveCommand.AddPoint(modifiedXCoord, modifiedYCoord);
               updatedMoveCommand.PrepareOutputString();

               modifiedCommands->push_back(penCommand);
               modifiedCommands->push_back(updatedMoveCommand);
            }
            else if(modifiedCommands->size() > 0) //Still out of bounds or still in bounds
            {
               AMoveCommand updatedMoveCommand;
               updatedMoveCommand.AddPoint(modifiedXCoord, modifiedYCoord);
               updatedMoveCommand.PrepareOutputString();

               modifiedCommands->push_back(updatedMoveCommand);
            }

            pair<int, int> point(currentXCoord, currentYCoord);
            points.push_back(point);

            //If we find an OP Code, we are done reading in coordinate points
            innerOpCodeCheck = input.substr(0, 2);
         }

         AMoveCommand moveCommand(points);
         moveCommand.PrepareOutputString();

         //If we went out of bounds, add the modified commands; Otherwise, show all points in one move command
         if (modifiedCommands->size() > 0)
         {
            commands->insert(commands->end(), modifiedCommands->begin(),
               modifiedCommands->end());
         }
         else
         {
            commands->push_back(moveCommand);
         }

         modifiedCommands->clear();
         delete modifiedCommands;
         modifiedCommands = NULL;
      }
      else //We have found a garbage value; ignoring the first character and continuing
      {
         input = input.substr(1);
      }
   }

   return commands;
}

int AParser::FindXPointIntersectingBounds(int x1, int y1, int x2, int y2)
{
   //Using the fact that the ratio of the length of sides in two triangles are the same 
   //(x2-x1)/(MAX_COORD - x1) = (y2-y1)/(X-y1); X being the value we are looking for

   //Need to figure out where on the coordinate plane the y2 value is to determine 
   //whether to use the MIN_COORD or MAX_COORD value
   int minOrMaxCoord = y2 < 0 ? kMIN_VALID_COORDINATE : kMAX_VALID_COORDINATE;

   double yRatio = (double)(y2 - y1) / (minOrMaxCoord - y1);
   int intersectionPoint = round( ((x2 - x1) / yRatio) + x1 );

   return intersectionPoint;
}

int AParser::FindYPointIntersectingBounds(int x1, int y1, int x2, int y2)
{
   //Using the fact that the ratio of the length of sides in two triangles are the same
   //(y2-y1)/(MAX_COORD - y1) = (x2-x1)/(Y-x1); Y being the value we are looking for

   //Need to figure out where on the coordinate plane the y2 value is to 
   //determine whether to use the MIN_COORD or MAX_COORD value
   int minOrMaxCoord = x2 < 0 ? kMIN_VALID_COORDINATE : kMAX_VALID_COORDINATE;

   double xRatio = (double)(x2 - x1) / (minOrMaxCoord - x1);
   int intersectionPoint = round( ((y2 - y1) / xRatio) + y1 );

   return intersectionPoint;
}

bool AParser::IsStringOpCode(string input)
{
   bool isOpCode = false;

   if (input == kOP_CODE_CLR || input == kOP_CODE_CO	|| 
      input == kOP_CODE_MV || input == kOP_CODE_PEN)
   {
      isOpCode = true;
   }

   return isOpCode;
}

int AParser::CalcDistanceToNextOpCode(string input)
{
   int distToNextClr = input.find(kOP_CODE_CLR);
   distToNextClr = distToNextClr == string::npos ? INT_MAX : distToNextClr;

   int distToNextCo = input.find(kOP_CODE_CO);
   distToNextCo = distToNextCo == string::npos ? INT_MAX : distToNextCo;

   int distToNextPen = input.find(kOP_CODE_PEN);
   distToNextPen = distToNextPen == string::npos ? INT_MAX : distToNextPen;

   int distToNextMv = input.find(kOP_CODE_MV);
   distToNextMv = distToNextMv == string::npos ? INT_MAX : distToNextMv;

   int minDist = min(distToNextClr, min(distToNextCo, min(distToNextPen, distToNextMv)));

   return minDist;
}