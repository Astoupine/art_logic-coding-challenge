Time taken:
230min - Complete logic, working program
50min - Conforming to A&L style guide and cleaning up code
Total - 280min (4h 40min)

Input file-
InputData_Part2.txt, located in the same directory as the app
Each input is on its own line, program reads input one at a time

Output file-
OutputCommands.txt