// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#include <fstream>
#include <iostream>

#include "Parser.h"

using namespace std;

int main()
{
   //Get input data from file 
   string line;
   ifstream inFile("InputData_Part2.txt");
   ofstream outFile;

   if (inFile.is_open())
   {
      AParser parser;
      outFile.open("OutputCommands.txt");

      while (getline(inFile, line))
      {
         outFile << "Input Data: " << line << '\n';
         vector<ACommand>* commands = parser.Parse(line);

         if (commands != NULL)
         {
            for (int i = 0; i < commands->size(); i++)
            {
               outFile << commands->at(i).Print();
            }

            commands->clear();
            delete commands;
            commands = NULL;

            outFile << '\n';
         }
         else
         {
            outFile << "Error parsing commands \n";
         }
      }
      inFile.close();
      outFile.close();
   }
   else
   {
      cout << "Error: Unable to open input data file.";
   }
}
