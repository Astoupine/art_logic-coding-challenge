// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#pragma once

#include <iomanip>
#include <sstream>

using namespace std;

////////////////////////////////
/*
	@class Codec
	@brief Simple class to hold the Encode and Decode functions
*/
///////////////////////////////

class ACodec {
public:
	/**
	* encodes a signed integer to a string
	* @param val value to encode
	* @return string encoded string
	*/
	string Encode(int val);
	/**
	* decodes a given hex value (string) into an integer
	* @param hiByte first two digits of the hex value to decode
	* @param loByte last two digits of the hex value to decode
	* @return int decoded value
	*/
	int Decode(string hiByte, string loByte);
};