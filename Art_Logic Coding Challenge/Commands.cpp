// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#include "Commands.h"

using namespace std;

AColorCommand::AColorCommand()
{
   //Default to black
   fRedValue = 0;
   fGreenValue = 0;
   fBlueValue = 0;
   fAlphaValue = 255;
}

AColorCommand::AColorCommand(int redValue, int greenValue, 
   int blueValue, int alphaValue)
{
   fRedValue = redValue;
   fGreenValue = greenValue;
   fBlueValue = blueValue;
   fAlphaValue = alphaValue;
}

void AMoveCommand::AddPoint(int x, int y)
{
   pair<int, int> newPoint(x, y);
   fPoints.push_back(newPoint);
}

void AClearCommand::PrepareOutputString()
{
   this->SetOutput("CLR; \n");
}

void APenCommand::PrepareOutputString()
{
   if (fIsPenDown)
   {
      this->SetOutput("PEN DOWN; \n");
   }
   else
   {
      this->SetOutput("PEN UP; \n");
   }
}

void AColorCommand::PrepareOutputString()
{
   stringstream ss;
   ss << "CO " << fRedValue << ' ' << fGreenValue << ' ' 
      << fBlueValue << ' ' << fAlphaValue << "; \n";

   this->SetOutput(ss.str());
}

void AMoveCommand::PrepareOutputString()
{
   stringstream ss;
   ss << "MV ";

   for (int i = 0; i < fPoints.size(); i++)
   {
      //If this is the last one, add a semicolon and newline
      if (i == fPoints.size() - 1)
      {
	      ss << "(" << fPoints[i].first << ", " << fPoints[i].second << "); \n";
      }
      else
      {
	      ss << "(" << fPoints[i].first << ", " << fPoints[i].second << ") ";
      }
   }

   this->SetOutput(ss.str());
}