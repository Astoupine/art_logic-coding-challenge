// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#include "Codec.h"

using namespace std;

string ACodec::Encode(int val)
{
	int translatedValue = val + 8192;

	//Need to represent this 14 bit value in 16 bits - mask for lower 7 and upper 7 bits
	int lowerBits = translatedValue & 0x7F;//00000000 01111111 Grabbing all of the 'L' bits
	int upperBits = translatedValue & 0x3F80;//00111111 10000000 Grabbing all of the 'H' bits

	//Need to shift the upperBits value left one to match format (00HHHHHH H...) -> (0HHHHHHH 0..)
	upperBits = upperBits << 1;

	int encodedInt = lowerBits + upperBits;

	stringstream ss;
	ss << hex << encodedInt;

	return ss.str();
}

int ACodec::Decode(string hiByte, string loByte)
{
	string hexVal = hiByte + loByte;
	int encodedInt = 0;

	std::stringstream ss;
	ss << std::hex << hexVal;
	ss >> encodedInt;

	int lowerBits = encodedInt & 0x7F;//00000000 01111111 Grabbing all of the 'L' bits
	int upperBits = encodedInt & 0x7F00;//01111111 00000000 Grabbing all of the 'H' bits

	//Need to shift the upperBits value right one to match format  (0HHHHHHH 0..) -> (00HHHHHH H...)
	upperBits = upperBits >> 1;

	int translatedInt = lowerBits + upperBits;

	int result = translatedInt - 8192;
	return result;
}