Time taken - 1.5hrs coding, 0.5 hours going through styleguide

Input file-
InputData_Part1.txt, located in the same directory as the app
Each input is on its own line, program reads input one at a time
Hex values are surrounded by quotes to differentiate them from integers

Codec class: 
Simple class to hold the Encode and Decode functions
Encode - encodes a signed integer to a string
	* @param val value to encode
	* @return string encoded string
Decode - decodes a given hex value (string) into an integer
	* @param hiByte first two digits of the hex value to decode
	* @param loByte last two digits of the hex value to decode
	* @return int decoded value