// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#pragma once

#include <iomanip>
#include <sstream>
#include <vector>

#include "Constants.h"

using namespace std;

////////////////////////////////
/*
   @class ACommand
   @brief Parent class which all other command classes derive from
   This is to make it easier to put all commands into the same vector
   while still maintaining properties exclusive to each command
*/
///////////////////////////////

class ACommand {
public:
   /**
   * sets the output string that the command will print to file
   * @param string text to display in output file
   * @return void
   */
   void SetOutput(string output) { fOutput = output; }
   /**
   * parent function to prepare the output string in accordance with
     each specific command's needs
   * @return void
   */
   virtual void PrepareOutputString() { fOutput = ""; };
   /**
   * returns the output string that has been prepared earlier
   * @return string text to output to file
   */
   string Print() { return fOutput; };

private:
   string fOutput; /* string to display on the output file */
};

////////////////////////////////
/*
   @class AClearCommand
   @brief Derived from ACommand class but specific to the CLR command
*/
///////////////////////////////

class AClearCommand : public ACommand {
public:
   /**
   * format the output string in accordance with the CLR command
   * @return void
   */
   void PrepareOutputString();
};

////////////////////////////////
/*
   @class APenCommand
   @brief Derived from ACommand class but specific to the PEN command
*/
///////////////////////////////

class APenCommand : public ACommand {
public:
   //Constructor
   APenCommand(int penDown) { fIsPenDown = penDown; };
   /**
   * format the output string in accordance with the PEN command
   * @return void
   */
   void PrepareOutputString();

private:
   int fIsPenDown; /* shows whether the pen is up (0) or down (not 0) */
};

////////////////////////////////
/*
   @class AColorCommand
   @brief Derived from ACommand class but specific to the CO command
*/
///////////////////////////////

class AColorCommand : public ACommand {
public:
   //Constructors
   AColorCommand();
   AColorCommand(int redValue, int greenValue, int blueValue, int alphaValue);
   /**
   * format the output string in accordance with the CO command
   * @return void
   */
   void PrepareOutputString();

private:
   int fRedValue; /* RGBA values of the color */
   int fGreenValue;
   int fBlueValue;
   int fAlphaValue;
};

////////////////////////////////
/*
   @class AMoveCommand
   @brief Derived from ACommand class but specific to the MO command
*/
///////////////////////////////

class AMoveCommand : public ACommand {
public:
   //Constructors
   AMoveCommand() {}
   AMoveCommand(vector<pair<int, int>> points) { fPoints = points; }
   /**
   * adds an x,y point to the command's list of points to move to
   * @param x X value of the point
   * @param y Y value of the point
   * @return void
   */
   void AddPoint(int x, int y);
   /**
   * format the output string in accordance with the MV command
   * @return void
   */
   void PrepareOutputString();

private:
   vector<pair<int, int>> fPoints; /* vector to hold all of the command's points to move to */
};