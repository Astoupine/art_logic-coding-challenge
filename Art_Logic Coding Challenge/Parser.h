// Copyright (c) 2019 Art & Logic, Inc. All Rights Reserved.

#pragma once

#include <algorithm>
#include <iomanip>
#include <sstream>

#include "Codec.h"
#include "Commands.h"

using namespace std;

////////////////////////////////
/*
	@class AParser
	@brief Simple class to take care of parsing the input, 
   decoding each command parameter, and generating commands
*/
///////////////////////////////

class AParser {
public:
   /**
   * main parsing loop
   * @param input string to parse and turn into commands
   * @return vector<ACommand>* pointer to a vector of commands
   */
   vector<ACommand>* Parse(string input);
   /**
   * calculates the X intercept on the edge of the drawing plane
     when the pen goes out of bounds
   * @param x1 x coordinate of the previous position of the pen
   * @param y1 y coordinate of the previous position of the pen
   * @param x2 x coordinate of the out-of-bounds position of the pen
   * @param y2 y coordinate of the out-of-bounds position of the pen
   * @return int value of the X intercept
   */
   int FindXPointIntersectingBounds(int x1, int y1, int x2, int y2);
   /**
   * calculates the Y intercept on the edge of the drawing plane
     when the pen goes out of bounds
   * @param x1 x coordinate of the previous position of the pen
   * @param y1 y coordinate of the previous position of the pen
   * @param x2 x coordinate of the out-of-bounds position of the pen
   * @param y2 y coordinate of the out-of-bounds position of the pen
   * @return int value of the Y intercept
   */
   int FindYPointIntersectingBounds(int x1, int y1, int x2, int y2);
   /**
   * sees if the input string matches any of the current OP Codes
   * @param input string to try to match against OP Codes
   * @return bool true if we found a match, false otherwise
   */
   bool IsStringOpCode(string input);
   /**
   * calculates how many characters are in between the start of the
     string and the next OP Code
   * @param input string to calculate the position from
   * @return int distance to the closest OP Code
   */
   int CalcDistanceToNextOpCode(string input);
};